package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Deze class helpt met het lezen van een inputstream van een socket. Het gaat
 * ervan uit dat listeners van deze thread graag op de hoogte gehouden willen
 * worden als er een regel tekst (een reekst ASCII karakters, gevolgd door een
 * 'enter' teken (\n)) is ontvangen.
 * 
 * Daarnaast worden listeners op de hoogte gebracht als de inputstream werd
 * gesloten.
 */
public class ReaderThread extends Thread {

	private BufferedReader in;

	private ChatConnection connection;

	public ReaderThread(ChatConnection connection) throws IOException {
		this.connection = connection;
		in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		this.setName(connection.toString());
	}

	/**
	 * Deze methode wordt ��nmalig door een apart proces uitgevoerd. Het start
	 * een loop die de inputstream leest, totdat er null's worden ontvangen, of
	 * een IOException wordt gegenereert.
	 */
	public void run() {
		String s = "";
		System.out.println("Reader starts listening to " + getName());
		try {
			while (s != null) {
				s = in.readLine();
				if (s!=null)
					connection.receiveMessage(s);
			}
		} catch (IOException e) {
		}
		System.out.println("Closing " + getName());
	}

}
