package common;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ChatConnection {

	private Socket socket;

	private ChatListener listener;

	private ReaderThread reader;

	public ChatConnection(Socket socket) {
		super();
		this.socket = socket;
		try {
			reader = new ReaderThread(this);
			reader.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void receiveMessage(String s) {
		if (listener != null)
			listener.onMessageReceived(this, s);
	}

	public InputStream getInputStream() throws IOException {
		return socket.getInputStream();
	}

	public void sendMessage(String message) throws IOException {
		message += '\n';
		socket.getOutputStream().write(message.getBytes());
		socket.getOutputStream().flush();
	}

	public ChatListener getListener() {
		return listener;
	}

	public void setListener(ChatListener listener) {
		this.listener = listener;
	}

	public String toString() {
		return "Connection to " + socket.getInetAddress();
	}

	public void close() throws IOException {
		socket.close();
	}

}
