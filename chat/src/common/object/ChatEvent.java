package common.object;

import java.io.Serializable;

public class ChatEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum Type {
		CLIENT_CONNECTED, CLIENT_DISCONNECTED, CLIENT_PROPERTYCHANGE, MESSAGE, ERROR
	}
	
	private Type type;
	
	private String value;

	public ChatEvent(Type type, String value) {
		super();
		this.type = type;
		this.value = value;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Type getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
	
	
}
