package common.object;


public interface ChatEventListener {

	void onChatEventReceived(ChatEventConnection chatConnection, ChatEvent event);
}
