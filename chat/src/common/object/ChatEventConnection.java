package common.object;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ChatEventConnection {

	private Socket socket;
	
	private ChatEventListener listener;
	
	public ChatEventConnection(Socket socket) {
		super();
		this.socket = socket;
	}

	protected void receiveEvent(ChatEvent event) {
		listener.onChatEventReceived(this, event);	
	}
	
	public InputStream getInputStream() throws IOException {
		return socket.getInputStream();
	}

	public void sendMessage(String message) throws IOException {
		socket.getOutputStream().write(message.getBytes());
	}

	public String toString() {
		return "Connection to " + socket.getInetAddress();
	}
	
}
