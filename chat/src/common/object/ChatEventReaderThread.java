package common.object;

import java.io.IOException;
import java.io.ObjectInputStream;


public class ChatEventReaderThread extends Thread {

	private ObjectInputStream in;

	private ChatEventConnection connection;

	public ChatEventReaderThread(ChatEventConnection connection) throws IOException {
		this.connection = connection;
		in = new ObjectInputStream(connection.getInputStream());
		this.setName(connection.toString());
	}

	/**
	 * Deze methode wordt ��nmalig door een apart proces uitgevoerd. Het start
	 * een loop die de inputstream leest, totdat er null's worden ontvangen, of
	 * een IOException wordt gegenereert.
	 */
	public void run() {
		ChatEvent s = null;
		System.out.println("Reader starts listening to " + getName());
		try {
			while (s != null) {
				try {
					s = (ChatEvent) in.readObject();
				} catch (Exception e) {
					s = new ChatEvent(ChatEvent.Type.ERROR, e.getLocalizedMessage());
				}
				connection.receiveEvent(s);
			}
		} catch (Exception e) {
			s = new ChatEvent(ChatEvent.Type.ERROR, e.getLocalizedMessage());
			connection.receiveEvent(s);
		}
		System.out.println("Closing connection to " + getName());
	}

}
