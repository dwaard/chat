package server;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import common.ChatConnection;
import common.ChatListener;

public class MessageForwarder implements ChatListener {

	private List<ChatConnection> connections = new LinkedList<ChatConnection>();
	
	@Override
	public void onMessageReceived(ChatConnection chatConnection, String message) {
		System.out.println("Forwarding: " + message);
		for (ChatConnection con : connections)
			if (chatConnection!=con)
				try {
					con.sendMessage(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
	}

	public boolean add(ChatConnection arg0) {
		System.out.println("Accepted client: " + arg0);
		arg0.setListener(this);
		return connections.add(arg0);
	}

	
	
}
