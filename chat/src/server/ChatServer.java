package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import common.ChatConnection;

public class ChatServer {

	public static void main(String[] args) {
		MessageForwarder forwarder = new MessageForwarder();
		ServerSocket serverSocket=null;
		try {
			serverSocket = new ServerSocket(9999);
			while (true) {
				// Wacht hier op een nieuwe verbinding
				Socket clientSocket = serverSocket.accept();
				ChatConnection acceptedClient = new ChatConnection(clientSocket);
				forwarder.add(acceptedClient);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (serverSocket!=null)
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

}
