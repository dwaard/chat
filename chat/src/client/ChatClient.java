package client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import common.ChatConnection;
import common.ChatListener;

/**
 * Deze klasse bevat de functionaliteit van een client in de chatdemo.
 * 
 * 
 * @author waar0003
 * 
 */
public class ChatClient implements ChatListener {

	private boolean connected = false;

	private ChatConnection connection;

	private ChatClientFrame view; // Close coupling met de GUI... waarom

	// niet?:-D

	/**
	 * @param view
	 */
	public ChatClient(ChatClientFrame view) {
		super();
		this.view = view;
	}

	/**
	 * Returns the connected
	 * 
	 * @return the connected
	 */
	protected boolean isConnected() {
		return connected;
	}

	/**
	 * Maakt een verbinding met een chatserver.
	 * 
	 * @param hostname
	 * @param port
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void connect(String hostname, int port) throws UnknownHostException, IOException {
		if (connected)
			throw new IllegalStateException("Ben al verbonden");

		// Creeer de socket
		Socket socket = new Socket(hostname, port);
		connection = new ChatConnection(socket);
		connection.setListener(this);
		// Zet de state van de client en de gui.
		this.connected = true;
		view.appendLine("Connected to Server... (" + socket.getInetAddress() + ":" + port + ")");
		view.updateGUI();
	}

	/**
	 * Wordt uitgevoerd als de reader-thread een regel tekst van de server heeft
	 * ontvangen.
	 */
	@Override
	public void onMessageReceived(ChatConnection chatConnection, String message) {
		view.appendLine(message);
	}

	/**
	 * Verzend een regel tekst naar de server
	 * 
	 * @param line
	 *            de tekst die moet worden verstuurd.
	 */
	public void sendText(String line) {
		try {
			connection.sendMessage(line);
		} catch (IOException e) {
			view.appendLine("Exception: " + e.getLocalizedMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "ChatClient: ";
		if (connected)
			result += "verbonden";
		else
			result += "nog niet verbonden";
		return result;
	}

	public void disconnect() throws IOException {
		connection.close();
		this.connected = false;
		view.updateGUI();		
	}


}