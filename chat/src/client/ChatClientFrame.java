package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

/**
 * Deze klasse representeert de GUI van een chat-client applicatie. Deze GUI
 * controleert een ChatClient object. Door de 'main' methode van deze klasse te
 * starten wordt een clientapplicatie gestart.
 * 
 * In deze client kan verbinding gemaakt worden met een server, en is een stukje
 * chat-functionaliteit ge�mplementeerd.
 * 
 * @author waar0003
 * 
 */
public class ChatClientFrame extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2695091218927360269L;

	private ChatClient client;

	private JTextField jtfIPAdress;

	private JLabel jLabel2;

	private JTextArea jtaText;

	private JTextField jtfInput;

	private JScrollPane jScrollPane1;

	private JFormattedTextField jftfPort;

	private JButton jbConnect;

	private JPanel jPanel1;

	private JButton jbSend;

	private JPanel jpControl;

	private JLabel jLabel1;

	private JPanel jpProperties;

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ChatClientFrame inst = new ChatClientFrame();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	/**
	 * Constructorretje...
	 */
	public ChatClientFrame() {
		super();
		initGUI();
		client = new ChatClient(this);
		this.updateGUI();
	}

	/*
	 * Handler voor het window-closing event.
	 */
	private void thisWindowClosing(WindowEvent evt) {
		System.exit(0);
	}

	/**
	 * Deze methode wordt aangeroepen als er een regel text van de server is
	 * ontvangen.
	 */
	public void appendLine(String line) {
		jtaText.append(line + "\n");
		jtaText.setCaretPosition(jtaText.getText().length());
	}

	/*
	 * Handler voor de connect-button
	 */
	private void jbConnectActionPerformed(ActionEvent evt) {
		if (!client.isConnected()) {
			String host = this.jtfIPAdress.getText();
			int port = ((Integer) this.jftfPort.getValue()).intValue();
			try {
				this.client.connect(host, port);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
		} else {
			try {
				client.disconnect();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, e.getMessage());
			}
		}
	}

	/*
	 * Handler voor de send-button
	 */
	private void sendActionPerformed(ActionEvent evt) {
		this.client.sendText(this.jtfInput.getText());
		this.jtfInput.setText("");
	}

	/**
	 * Deze methode wordt aangeroepenals op ��n of andere manier (behalve
	 * wanneer er een regel tekst van de server is gelezen) de toestand van de
	 * client is gewijzigd. Dit kan zijn wanneer de verbinding is geopend of
	 * gesloten.
	 */
	public void updateGUI() {
		this.setTitle(client.toString());
		boolean c = client.isConnected();
		this.jtfIPAdress.setEnabled(!c);
		this.jftfPort.setEnabled(!c);
		if (c)
			this.jbConnect.setText("Disconnect");
		else
			this.jbConnect.setText("Connect");
		this.jtaText.setEnabled(c);
		this.jtfInput.setEnabled(c);
		this.jbSend.setEnabled(c);
	}

	/*
	 * Init de GUI...
	 */
	private void initGUI() {
		try {
			try {
				setPreferredSize(new Dimension(400, 300));
				this.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent evt) {
						thisWindowClosing(evt);
					}
				});
				JPanel contentpane = new JPanel();
				BorderLayout thisLayout = new BorderLayout();
				contentpane.setLayout(thisLayout);
				this.setContentPane(contentpane);
				{
					jpProperties = new JPanel();
					GridBagLayout jpPropertiesLayout = new GridBagLayout();
					contentpane.add(jpProperties, BorderLayout.NORTH);
					jpPropertiesLayout.rowWeights = new double[] { 0.1, 0.1 };
					jpPropertiesLayout.rowHeights = new int[] { 7, 7 };
					jpPropertiesLayout.columnWeights = new double[] { 0.1, 0.2 };
					jpPropertiesLayout.columnWidths = new int[] { 7, 7 };
					jpProperties.setLayout(jpPropertiesLayout);
					jpProperties.setBorder(BorderFactory.createTitledBorder(null,
							"Instellingen verbinding", TitledBorder.LEADING,
							TitledBorder.DEFAULT_POSITION));
					{
						jLabel1 = new JLabel();
						jpProperties.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jLabel1.setText("IP-adres");
					}
					{
						jtfIPAdress = new JTextField();
						jpProperties.add(jtfIPAdress, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jtfIPAdress.setText("127.0.0.1");
					}
					{
						jLabel2 = new JLabel();
						jpProperties.add(jLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jLabel2.setText("Poort");
					}
					{
						jbConnect = new JButton();
						jpProperties.add(jbConnect, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jbConnect.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								jbConnectActionPerformed(evt);
							}
						});
						jbConnect.setText("Connect");
					}
					{
						jftfPort = new JFormattedTextField(new Integer(9999));
						jpProperties.add(jftfPort, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
					}
				}
				{
					jPanel1 = new JPanel();
					BorderLayout jPanel1Layout = new BorderLayout();
					jPanel1.setLayout(jPanel1Layout);
					contentpane.add(jPanel1, BorderLayout.CENTER);
					jPanel1.setPreferredSize(new java.awt.Dimension(400, 189));
					jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Server antwoorden",
							TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION));
					{
						jScrollPane1 = new JScrollPane();
						jPanel1.add(jScrollPane1, BorderLayout.CENTER);
						jScrollPane1.setPreferredSize(new java.awt.Dimension(382, 110));
						{
							jtaText = new JTextArea();
							jScrollPane1.setViewportView(jtaText);
							jtaText
									.setBorder(BorderFactory
											.createEtchedBorder(BevelBorder.LOWERED));
						}
					}
				}
				{
					jpControl = new JPanel();
					GridBagLayout jpControlLayout = new GridBagLayout();
					jpControlLayout.rowWeights = new double[] { 0.1 };
					jpControlLayout.rowHeights = new int[] { 7 };
					jpControlLayout.columnWeights = new double[] { 0.1, 0.0 };
					jpControlLayout.columnWidths = new int[] { 7, 7 };
					contentpane.add(jpControl, BorderLayout.SOUTH);
					jpControl.setLayout(jpControlLayout);
					jpControl.setBorder(BorderFactory.createTitledBorder("Verzenden"));
					{
						jtfInput = new JTextField();
						jpControl.add(jtfInput, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jtfInput.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								sendActionPerformed(evt);
							}
						});
					}
					{
						jbSend = new JButton();
						jpControl.add(jbSend, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,
										0, 0, 0), 0, 0));
						jbSend.setText("Send");
						jbSend.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent evt) {
								sendActionPerformed(evt);
							}
						});
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			pack();
			setSize(400, 300);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
